import tinify
import os
from os import listdir
from os.path import isfile, join

#### Paste your API key ####
tinify.key = "Q4eHcX1kR9aNpUd42tJdVSfpxHmInf2P"


init_path = 'img_to_compress/'
result_path = 'img_compressed/'
initial_size = 0
final_size = 0

for file in listdir(init_path):

	file_path = (init_path+file)
	result_file_path = (result_path+file)

	try:
		source = tinify.from_file(file_path)
		print(file_path)
		source.to_file(result_file_path)
	except tinify.errors.ClientError:
		print(file_path, " Warning: It is not a valid file to compress, it must have an extension '.jpg' or '.png' ")
		continue


for f in os.listdir('img_to_compress/'):
	initial_size += os.stat('img_to_compress/'+f).st_size

for f in os.listdir('img_compressed/'):
	final_size += os.stat('img_compressed/'+f).st_size



initial_size 	= initial_size / 1000000.0
final_size  	= final_size / 1000000.0
total = initial_size - final_size

print('Initial Folder Size:')
print('%.2fM' % initial_size)

print('Final Folder Size')
print('%.2fM' % final_size)

print('Saved %.2fM' % total)
